<?php
namespace Wwwision\NeosCon\GraphQl\RootTypes;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use Neos\Flow\Annotations as Flow;
use Wwwision\GraphQL\TypeResolver;
use Wwwision\NeosCon\Domain\Service\AttendeeService;

class Mutation extends ObjectType
{

    /**
     * @Flow\Inject
     * @var AttendeeService
     */
    protected $attendeeService;

    /**
     * @param TypeResolver $typeResolver
     */
    public function __construct(TypeResolver $typeResolver)
    {
        return parent::__construct([
            'name' => 'Mutations',
            'fields' => [
                'addSessionLike' => [
                    'type' => Type::boolean(),
                    'args' => [
                        'sessionId' => ['type' => Type::nonNull(Type::string())],
                    ],
                    'resolve' => function ($_, $args) {
                        $this->attendeeService->addSessionLikeForAuthenticatedAttendee($args['sessionId']);
                        return true;
                    },
                ],
                'revokeSessionLike' => [
                    'type' => Type::boolean(),
                    'args' => [
                        'sessionId' => ['type' => Type::nonNull(Type::string())],
                    ],
                    'resolve' => function ($_, $args) {
                        $this->attendeeService->revokeSessionLikeForAuthenticatedAttendee($args['sessionId']);
                        return true;
                    },
                ],
            ],
        ]);
    }
}