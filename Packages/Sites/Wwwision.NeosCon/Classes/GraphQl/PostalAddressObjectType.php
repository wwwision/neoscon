<?php
namespace Wwwision\NeosCon\GraphQl;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use Wwwision\GraphQL\TypeResolver;
use Wwwision\NeosCon\GraphQl\Scalars\DateTime;

final class PostalAddressObjectType extends ObjectType
{

    /**
     * @param TypeResolver $typeResolver
     */
    public function __construct(TypeResolver $typeResolver)
    {
        return parent::__construct([
            'name' => 'PostalAddress',
            'fields' => [
                'streetAddress' => ['type' => Type::string()],
                'postOfficeBoxNumber' => ['type' => Type::string()],
                'addressLocality' => ['type' => Type::string()],
                'postalCode' => ['type' => Type::string()],
                'addressCountry' => ['type' => Type::string()],
                'latitude' => ['type' => Type::float()],
                'longitude' => ['type' => Type::float()],
            ]
        ]);
    }
}