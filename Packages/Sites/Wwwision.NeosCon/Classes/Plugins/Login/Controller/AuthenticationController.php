<?php
namespace Wwwision\NeosCon\Plugins\Login\Controller;

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Mvc\ActionRequest;
use Neos\Flow\Security\Authentication\Controller\AbstractAuthenticationController;

final class AuthenticationController extends AbstractAuthenticationController
{
    /**
     * @param ActionRequest $originalRequest The request that was intercepted by the security framework, NULL if there was none
     * @return string
     */
    protected function onAuthenticationSuccess(ActionRequest $originalRequest = null)
    {
        $this->addFlashMessage('You are now signed-in!', 'Success');
        $this->redirect('login');
    }

    public function logoutAction()
    {
        parent::logoutAction();
        $this->addFlashMessage('You are now signed-out!', 'Success');
        $this->redirect('login');
    }
}