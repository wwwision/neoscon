<?php
namespace Wwwision\NeosCon\Eel\Helper;

use Neos\Eel\ProtectedContextAwareInterface;
use Neos\Flow\Annotations as Flow;
use Wwwision\NeosCon\Domain\Service\ScheduleService;
use Wwwision\NeosCon\Domain\Service\VenueService;

/**
 * @Flow\Scope("singleton")
 */
class ScheduleHelper implements ProtectedContextAwareInterface
{

    /**
     * @var ScheduleService
     */
    private $scheduleService;

    public function __construct(ScheduleService $scheduleService)
    {
        $this->scheduleService = $scheduleService;
    }

    /**
     * @param string $methodName
     * @param array $arguments
     * @return mixed
     */
    public function __call(string $methodName, array $arguments)
    {
        $methodName = 'get' . ucfirst($methodName);
        return call_user_func_array([$this->scheduleService, $methodName], $arguments);
    }

    /**
     * @param string $methodName
     * @return boolean
     */
    public function allowsCallOfMethod($methodName)
    {
        return true;
    }
}