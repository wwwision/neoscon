<?php
namespace Wwwision\NeosCon\Eel;

use Neos\Flow\Annotations as Flow;
use Neos\ContentRepository\Domain\Model\NodeInterface;
use Neos\ContentRepository\Eel\FlowQueryOperations\FilterOperation as ContentRepositoryFilterOperation;

/**
 * A custom filter operation that allows for filtering reference(s) node properties:
 *
 * $flowQuery->find('[instanceof Some:NodeType]')->filter(reference_property="<reference_identifier>")->get();
 * $flowQuery->find('[instanceof Some:NodeType]')->filter(references_property*="<reference_identifier>")->get();
 */
class FilterOperation extends ContentRepositoryFilterOperation
{

    /**
     * {@inheritdoc}
     *
     * @var integer
     */
    protected static $priority = 110;

    /**
     * Evaluate an operator
     *
     * @param mixed $value
     * @param string $operator
     * @param mixed $operand
     * @return boolean
     */
    protected function evaluateOperator($value, $operator, $operand)
    {
        if ($operator === '*=' && is_array($value) && is_string($operand)) {
            foreach ($value as $node) {
                if (!$node instanceof NodeInterface) {
                    return false;
                }
                if ($node->getIdentifier() === $operand) {
                    return true;
                }
            }
            return false;
        }
        if ($operator === '=' && $value instanceof NodeInterface && is_string($operand)) {
            return $value->getIdentifier() === $operand;
        }
        return parent::evaluateOperator($value, $operator, $operand);
    }

}