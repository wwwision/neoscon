<?php
namespace Wwwision\NeosCon\Domain\Service;

use Neos\ContentRepository\Domain\Model\NodeInterface;
use Neos\Flow\Annotations as Flow;
use Wwwision\NeosCon\ContentRepository\Service\NodeServiceFactory;
use Wwwision\NeosCon\Domain\Model\Conference;
use Wwwision\NeosCon\Domain\Model\Room;
use Wwwision\NeosCon\Domain\Model\Venue;

/**
 * @Flow\Scope("singleton")
 */
final class VenueService
{
    /**
     * @var NodeServiceFactory
     */
    private $nodeServiceFactory;

    public function __construct(NodeServiceFactory $nodeServiceFactory)
    {
        $this->nodeServiceFactory = $nodeServiceFactory;
    }

    public function getVenueForNode(NodeInterface $node)
    {
        return new Venue($node);
    }

    /**
     * @param Conference $conference
     * @return Venue
     */
    public function getVenueByConference(Conference $conference)
    {
        $conferenceNode = $conference->getNode();
        $nodeService = $this->nodeServiceFactory->forNode($conferenceNode);
        $venueNode = $nodeService->getFirstNodeByType(Venue::getNodeTypeName(), $conferenceNode);
        if ($venueNode === null) {
            return null;
        }
        return $this->getVenueForNode($venueNode);
    }

    /**
     * @param Venue $venue
     * @return Room[]
     */
    public function getRoomsForVenue(Venue $venue): array
    {
        return $this->getRoomsForNode($venue->getNode());
    }

    /**
     * @param NodeInterface $node
     * @return Room[]
     */
    public function getRoomsForNode(NodeInterface $node): array
    {
        $nodeService = $this->nodeServiceFactory->forNode($node);
        $roomNodes = $nodeService->getNodesByType(Room::getNodeTypeName(), $node);

        return array_map(function(NodeInterface $node) { return new Room($node); }, $roomNodes);
    }

    public function getRoomForNode(NodeInterface $node): Room
    {
        return new Room($node);
    }

}