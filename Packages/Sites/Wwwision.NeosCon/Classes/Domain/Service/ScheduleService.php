<?php
namespace Wwwision\NeosCon\Domain\Service;

use Neos\ContentRepository\Domain\Model\NodeInterface;
use Neos\Flow\Annotations as Flow;
use Wwwision\NeosCon\ContentRepository\Service\NodeServiceFactory;
use Wwwision\NeosCon\Domain\Model\Conference;
use Wwwision\NeosCon\Domain\Model\Room;
use Wwwision\NeosCon\Domain\Model\Schedule;
use Wwwision\NeosCon\Domain\Model\Timeslot;
use Wwwision\NeosCon\Domain\Model\Venue;

/**
 * @Flow\Scope("singleton")
 */
final class ScheduleService
{
    /**
     * @var NodeServiceFactory
     */
    private $nodeServiceFactory;

    public function __construct(NodeServiceFactory $nodeServiceFactory)
    {
        $this->nodeServiceFactory = $nodeServiceFactory;
    }

    /**
     * @param Conference $conference
     * @return Schedule
     */
    public function getScheduleByConference(Conference $conference)
    {
        $conferenceNode = $conference->getNode();
        $nodeService = $this->nodeServiceFactory->forNode($conferenceNode);
        $scheduleNode = $nodeService->getFirstNodeByType(Schedule::getNodeTypeName(), $conferenceNode);
        if ($scheduleNode === null) {
            return null;
        }
        return new Schedule($scheduleNode);
    }

    /**
     * @param NodeInterface $node
     * @return Timeslot[]
     */
    public function getTimeslotsForNode(NodeInterface $node): array
    {
        $nodeService = $this->nodeServiceFactory->forNode($node);
        $timeslotNodes = $nodeService->getNodesByType(Timeslot::getNodeTypeName(), $node);

        return array_map(function(NodeInterface $node) { return new Timeslot($node); }, $timeslotNodes);
    }

}