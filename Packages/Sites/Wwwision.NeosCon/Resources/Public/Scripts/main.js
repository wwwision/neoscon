/*
 Verti by HTML5 UP
 html5up.net | @ajlkn
 Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
 */

(function ($) {

	skel.breakpoints({
		xlarge: '(max-width: 1680px)',
		large: '(max-width: 1280px)',
		medium: '(max-width: 980px)',
		small: '(max-width: 736px)'
	});

	$(function () {

		var $window = $(window),
			$body = $('body');

		// Disable animations/transitions until the page has loaded.
		$body.addClass('is-loading');

		$window.on('load', function () {
			$body.removeClass('is-loading');
		});

		// Fix: Placeholder polyfill.
		$('form').placeholder();

		// Prioritize "important" elements on medium.
		skel.on('+medium -medium', function () {
			$.prioritize(
				'.important\\28 medium\\29',
				skel.breakpoint('medium').active
			);
		});

		// Dropdowns.
		$('#nav > ul').dropotron({
			mode: 'fade',
			noOpenerFade: true,
			speed: 300
		});

		// Off-Canvas Navigation.

		// Navigation Toggle.
		$(
			'<div id="navToggle">' +
			'<a href="#navPanel" class="toggle"></a>' +
			'</div>'
		)
			.appendTo($body);

		// Navigation Panel.
		$(
			'<div id="navPanel">' +
			'<nav>' +
			$('#nav').navList() +
			'</nav>' +
			'</div>'
		)
			.appendTo($body)
			.panel({
				delay: 500,
				hideOnClick: true,
				hideOnSwipe: true,
				resetScroll: true,
				resetForms: true,
				side: 'left',
				target: $body,
				visibleClass: 'navPanel-visible'
			});

		// Fix: Remove navPanel transitions on WP<10 (poor/buggy performance).
		if (skel.vars.os == 'wp' && skel.vars.osVersion < 10)
			$('#navToggle, #navPanel, #page-wrapper')
				.css('transition', 'none');

	});

	$('.like-button').each(function () {
		var $likeButton = $(this),
			sessionId = $likeButton.data('session-id');

		$likeButton.addClass('loading');

		graphQlQuery = function (query, variables) {
			return $.ajax({
				url: '/graphql',
				dataType: 'json',
				method: 'POST',
				data: {
					query: query,
					variables: variables
				}
			}).fail(function (error) {
				// TODO: Error handling
			})
		};


		$likeButton.on('click', function (event) {
			event.preventDefault();

			$likeButton.addClass('loading');
			if ($likeButton.hasClass('active')) {
				graphQlQuery('mutation ($sessionId: String!) { revokeSessionLike(sessionId: $sessionId) }', {sessionId: sessionId})
					.done(function () {
						$likeButton.removeClass('active loading');
					});
			} else {
				graphQlQuery('mutation ($sessionId: String!) { addSessionLike(sessionId: $sessionId) }', {sessionId: sessionId})
					.done(function () {
						$likeButton.addClass('active');
						$likeButton.removeClass('loading');
					});
			}
		});

		graphQlQuery('query ($sessionId:String!) { attendee { likesSession(sessionId:$sessionId) } }', {sessionId: $likeButton.data('session-id')})
			.done(function (result) {
				if (result.data.attendee.likesSession) {
					$likeButton.addClass('active');
				}
				$likeButton.removeClass('loading');
			});

	});


})(jQuery);

function initializeMaps() {
	$('.map').each(function () {
		var $mapContainer = $(this);
		var map = new google.maps.Map($mapContainer.find('.canvas').get(0), {
			center: {lat: -34.397, lng: 150.644},
			scrollwheel: true,
			zoom: 14,
			styles: [
				{featureType: 'all', stylers: [{saturation: -80}]}
			]
		});

		$mapContainer.find('.marker').each(function () {
			var geoCoordinates = {lat: $(this).data('latitude'), lng: $(this).data('longitude')};
			new google.maps.Marker({
				map: map,
				position: geoCoordinates,
				title: $(this).text()
			});
			map.setCenter(geoCoordinates);
		});
	});
}