<?php
namespace Wwwision\NeosCon\ContentRepository;

use Neos\ContentRepository\Domain\Model\NodeInterface;
use Wwwision\NeosCon\Domain\Model\Session;

final class SessionFilter implements NodeFilterInterface
{
    /**
     * @var string
     */
    public $speakerId;

    /**
     * @var string
     */
    public $sessionType;

    public function __construct(array $vars = [])
    {
        array_walk($vars, function($filterValue, $filterName) {
            if (!property_exists($this, $filterName)) {
                throw new \InvalidArgumentException(sprintf('The property "%s" does not exist in class "%s"!', $filterName, get_class($this)), 1487593640);
            }
            $this->{$filterName} = $filterValue;
        });
    }

    public function getNodeType(): string
    {
        return Session::getNodeTypeName();
    }

    public function getFilterString(): string
    {
        $filter = '';
        if (strlen($this->speakerId) > 0) {
            $filter .= '[speakers*="' . $this->speakerId . '"]';
        }
        if (strlen($this->sessionType) > 0) {
            $filter .= '[sessionType="' . $this->sessionType . '"]';
        }
        return $filter;
    }
}