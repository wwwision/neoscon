<?php
namespace Wwwision\NeosCon\GraphQl;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use Neos\Flow\Annotations as Flow;
use Wwwision\GraphQL\TypeResolver;
use Wwwision\NeosCon\Domain\Model\Conference;
use Wwwision\NeosCon\Domain\Service\SessionService;
use Wwwision\NeosCon\Domain\Service\VenueService;
use Wwwision\NeosCon\GraphQl\Scalars\DateTimeScalar;
use Wwwision\NeosCon\GraphQl\Scalars\SessionTypeEnum;

final class ConferenceObjectType extends ObjectType
{

    /**
     * @var VenueService
     * @Flow\Inject
     */
    protected $venueService;

    /**
     * @var SessionService
     * @Flow\Inject
     */
    protected $sessionService;

    /**
     * @param TypeResolver $typeResolver
     */
    public function __construct(TypeResolver $typeResolver)
    {
        return parent::__construct([
            'name' => 'Conference',
            'fields' => [
                'title' => ['type' => Type::string()],
                'startDate' => ['type' => $typeResolver->get(DateTimeScalar::class)],
                'endDate' => ['type' => $typeResolver->get(DateTimeScalar::class)],

                'venue' => [
                    'type' => $typeResolver->get(VenueObjectType::class),
                    'resolve' => function (Conference $conference) {
                        return $this->venueService->getVenueByConference($conference);
                    },
                ],

                'sessions' => [
                    'type' => Type::listOf($typeResolver->get(SessionObjectType::class)),
                    'args' => [
                        'sessionType' => ['type' => $typeResolver->get(SessionTypeEnum::class)],
                    ],
                    'resolve' => function (Conference $conference, array $args) {
                        if (isset($args['sessionType'])) {
                            return $this->sessionService->getSessionsByConferenceAndSessionType($conference, $args['sessionType']);
                        }
                        return $this->sessionService->getSessionsByConference($conference);
                    },
                ],
            ]
        ]);
    }
}