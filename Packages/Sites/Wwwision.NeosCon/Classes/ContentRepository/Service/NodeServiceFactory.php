<?php
namespace Wwwision\NeosCon\ContentRepository\Service;

use Neos\Flow\Annotations as Flow;
use Neos\Neos\Domain\Service\ContentContext;
use Neos\ContentRepository\Domain\Model\NodeInterface;
use Neos\ContentRepository\Domain\Service\ContextFactoryInterface;

/**
 * @Flow\Scope("singleton")
 * @internal This factory is meant to be used by other services exclusively
 */
class NodeServiceFactory
{
    /**
     * @var ContextFactoryInterface
     */
    private $contextFactory;

    public function __construct(ContextFactoryInterface $contextFactory)
    {
        $this->contextFactory = $contextFactory;
    }

    /**
     * @param NodeInterface $node
     * @return NodeService
     */
    public function forNode(NodeInterface $node)
    {
        /** @var ContentContext $contentContext */
        $contentContext = $node->getContext();
        return new NodeService($contentContext);
    }

    public function create()
    {
        /** @var ContentContext $contentContext */
        $contentContext = $this->contextFactory->create();
        return new NodeService($contentContext);
    }

}