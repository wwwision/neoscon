# Neos Distribution for a dummy conference website presented at NeosCon 2017

## Installation

Check out this repository and install it via composer:

```
git clone https://gitlab.com/wwwision/neoscon.git neoscon
cd neoscon
composer install
```

navigate to http://localhost/setup (or whatever your local webserver's URL is) and
follow the steps of the Neos Setup Wizard.

To create an "attendee" user, execute the following command:

```
./flow attendee:create
```

and enter `fullName`, `username` and `password` when asked.
Afterwards you should be able to use the Frontend Login at http://localhost/login.html
with the specified credentials.