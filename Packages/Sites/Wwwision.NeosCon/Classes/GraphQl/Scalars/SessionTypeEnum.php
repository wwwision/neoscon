<?php
namespace Wwwision\NeosCon\GraphQl\Scalars;

use GraphQL\Type\Definition\EnumType;
use Wwwision\GraphQL\TypeResolver;
use Wwwision\NeosCon\Domain\Model\Session;

/**
 * Scalar type wrapper for \DateTimeInterface values
 */
class SessionTypeEnum extends EnumType
{

    /**
     * @param TypeResolver $typeResolver
     */
    public function __construct(TypeResolver $typeResolver)
    {
        return parent::__construct([
            'name' => 'SessionTypes',
            'values' => array_map(function($value) { return ['description' => $value]; }, Session::SESSION_TYPE_OPTIONS),
        ]);
    }
}