<?php
namespace Wwwision\NeosCon\Domain\Model;

use Neos\ContentRepository\Domain\Model\NodeInterface;
use Neos\Flow\Annotations as Flow;

final class Session extends AbstractNodeBasedModel
{

    const SESSION_TYPE_OPTIONS = [
        'keynote' => 'Keynote',
        'talk' => 'Standard talk',
    ];

    /**
     * @return Speaker[]
     */
    public function getSpeakers(): array
    {
        return array_map(function(NodeInterface $speakerNode) {
            return new Speaker($speakerNode);
        }, $this->node->getProperty('speakers'));
    }

}