<?php
namespace Wwwision\NeosCon\GraphQl;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use Neos\Flow\Annotations as Flow;
use Wwwision\GraphQL\TypeResolver;
use Wwwision\NeosCon\Domain\Model\Venue;
use Wwwision\NeosCon\Domain\Service\VenueService;

final class VenueObjectType extends ObjectType
{

    /**
     * @Flow\Inject
     * @var VenueService
     */
    protected $venueService;

    /**
     * @param TypeResolver $typeResolver
     */
    public function __construct(TypeResolver $typeResolver)
    {
        return parent::__construct([
            'name' => 'Venue',
            'fields' => [
                'title' => ['type' => Type::string()],
                'address' => ['type' => $typeResolver->get(PostalAddressObjectType::class)],

                'rooms' => [
                    'type' => Type::listOf($typeResolver->get(RoomObjectType::class)),
                    'resolve' => function(Venue $venue) {
                        return $this->venueService->getRoomsForVenue($venue);
                    },
                ]
            ]
        ]);
    }
}