<?php
namespace Wwwision\NeosCon\Domain\Service;

use Neos\ContentRepository\Domain\Model\NodeInterface;
use Neos\Flow\Annotations as Flow;
use Wwwision\NeosCon\ContentRepository\Service\NodeServiceFactory;
use Wwwision\NeosCon\Domain\Model\Conference;

/**
 * @Flow\Scope("singleton")
 */
final class ConferenceService
{
    /**
     * @var NodeServiceFactory
     */
    private $nodeServiceFactory;

    public function __construct(NodeServiceFactory $nodeServiceFactory)
    {
        $this->nodeServiceFactory = $nodeServiceFactory;
    }

    public function getConferenceForNode(NodeInterface $node): Conference
    {
        return new Conference($node);
    }

    /**
     * @return Conference[]
     */
    public function getConferences(): array
    {
        $nodeService = $this->nodeServiceFactory->create();
        $conferenceNodes = $nodeService->getNodesByType(Conference::getNodeTypeName());

        return array_map(function(NodeInterface $node) { return new Conference($node); }, $conferenceNodes);
    }

}