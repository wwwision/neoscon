<?php
namespace Wwwision\NeosCon\GraphQl;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use Neos\Flow\Annotations as Flow;
use Wwwision\GraphQL\TypeResolver;
use Wwwision\NeosCon\Domain\Model\Attendee;
use Wwwision\NeosCon\Domain\Service\AttendeeService;

final class AttendeeObjectType extends ObjectType
{

    /**
     * @Flow\Inject
     * @var AttendeeService
     */
    protected $attendeeService;

    /**
     * @param TypeResolver $typeResolver
     */
    public function __construct(TypeResolver $typeResolver)
    {
        return parent::__construct([
            'name' => 'Attendee',
            'fields' => [
                'likesSession' => [
                    'type' => Type::boolean(),
                    'args' => [
                        'sessionId' => ['type' => Type::nonNull(Type::string())],
                    ],
                    'resolve' => function (Attendee $attendee, $args) {
                        return $this->attendeeService->attendeeLikesSession($attendee, $args['sessionId']);
                    }
                ],
            ]
        ]);
    }
}