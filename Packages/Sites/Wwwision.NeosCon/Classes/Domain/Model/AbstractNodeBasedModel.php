<?php
namespace Wwwision\NeosCon\Domain\Model;

use Neos\Flow\Annotations as Flow;
use Sandstorm\CrudForms\Annotations as Crud;
use Neos\ContentRepository\Domain\Model\NodeInterface;

abstract class AbstractNodeBasedModel implements \ArrayAccess
{

    /**
     * The original Content Repository node that is the source for this DTO
     *
     * @var NodeInterface
     */
    protected $node;

    public function __construct(NodeInterface $node)
    {
        if ($node->getNodeType()->getName() !== static::getNodeTypeName()) {
            throw new \InvalidArgumentException(sprintf('Invalid node type "%s", expected "%s"', $node->getNodeType(), static::getNodeTypeName()), 1487349798);
        }
        $this->node = $node;
    }

    public static function getNodeTypeName(): string
    {
        return 'Wwwision.NeosCon:' . (new \ReflectionClass(get_called_class()))->getShortName();
    }

    public function getNode(): NodeInterface
    {
        return $this->node;
    }

    public function getIdentifier(): string
    {
        return $this->node->getIdentifier();
    }

    public function offsetExists($propertyName): bool
    {
        if ($this->node === null) {
            return false;
        }
        if (is_callable([$this, 'get' . ucfirst($propertyName)])) {
            return true;
        }
        return $this->node->hasProperty($propertyName);
    }

    public function offsetGet($propertyName)
    {
        if ($this->node === null) {
            return null;
        }
        if (is_callable([$this, 'get' . ucfirst($propertyName)])) {
            return call_user_func([$this, 'get' . ucfirst($propertyName)]);
        }
        $result = $this->node->getProperty($propertyName);
        if ($result instanceof NodeInterface) {
            return new static($result);
        }
        return $result;
    }

    public function offsetSet($offset, $value)
    {
        throw new \RuntimeException('This wrapper does not allow for mutation!', 1487349892);
    }

    public function offsetUnset($offset)
    {
        throw new \RuntimeException('This wrapper wrapper does not allow for mutation!', 1487349893);
    }

    /**
     * This is required in order to implicitly cast wrapped string types for example
     */
    function __toString(): string
    {
        return (string)$this->node;
    }


}