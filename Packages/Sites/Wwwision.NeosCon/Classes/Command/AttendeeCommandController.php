<?php
namespace Wwwision\NeosCon\Command;

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Cli\CommandController;
use Wwwision\NeosCon\Domain\Service\AttendeeService;

final class AttendeeCommandController extends CommandController
{
    /**
     * @Flow\Inject
     * @var AttendeeService
     */
    protected $attendeeService;

    public function createCommand(string $fullName, string $username, string $password)
    {
        $this->attendeeService->createAttendee($fullName, $username, $password);
        $this->outputLine('Added attendee "%s" (%s)', [$fullName, $username]);
    }
}