<?php
namespace Wwwision\NeosCon\Domain\Service;

use Neos\ContentRepository\Domain\Model\NodeInterface;
use Neos\Flow\Annotations as Flow;
use Wwwision\NeosCon\ContentRepository\Service\NodeServiceFactory;
use Wwwision\NeosCon\ContentRepository\SessionFilter;
use Wwwision\NeosCon\Domain\Model\Conference;
use Wwwision\NeosCon\Domain\Model\Session;
use Wwwision\NeosCon\Domain\Model\Speaker;

/**
 * @Flow\Scope("singleton")
 */
final class SessionService
{
    /**
     * @var NodeServiceFactory
     */
    private $nodeServiceFactory;

    public function __construct(NodeServiceFactory $nodeServiceFactory)
    {
        $this->nodeServiceFactory = $nodeServiceFactory;
    }

    public function getSpeakerForNode(NodeInterface $node): Speaker
    {
        return new Speaker($node);
    }

    /**
     * @param NodeInterface $node
     * @return Speaker[]
     */
    public function getSpeakersForNode(NodeInterface $node): array
    {
        $nodeService = $this->nodeServiceFactory->forNode($node);
        $speakerNodes = $nodeService->getNodesByType(Speaker::getNodeTypeName(), $node);

        return array_map(function (NodeInterface $node) {
            return new Speaker($node);
        }, $speakerNodes);
    }

    /**
     * @param Session $session
     * @return Speaker[]
     */
    public function getSpeakersForSession(Session $session): array
    {
        $nodeService = $this->nodeServiceFactory->forNode($session->getNode());
        $speakerNodes = $nodeService->getNodesByType(Speaker::getNodeTypeName(), $node);

        return array_map(function (NodeInterface $node) {
            return new Speaker($node);
        }, $speakerNodes);
    }

    public function getSessionForNode(NodeInterface $node): Session
    {
        return new Session($node);
    }

    /**
     * @param NodeInterface $node
     * @return Session[]
     */
    public function getSessionsForNode(NodeInterface $node): array
    {
        $nodeService = $this->nodeServiceFactory->forNode($node);
        $sessionNodes = $nodeService->getNodesByType(Session::getNodeTypeName());

        return array_map(function (NodeInterface $node) {
            return new Session($node);
        }, $sessionNodes);
    }

    /**
     * @param Speaker $speaker
     * @return Session[]
     */
    public function getSessionsForSpeaker(Speaker $speaker): array
    {
        $nodeService = $this->nodeServiceFactory->forNode($speaker->getNode());
        $sessionFilter = new SessionFilter(['speakerId' => $speaker->getIdentifier()]);
        $sessionNodes = $nodeService->getNodesByFilter($sessionFilter);

        return array_map(function (NodeInterface $node) {
            return new Session($node);
        }, $sessionNodes);
    }

    /**
     * @param Conference $conference
     * @return Session[]
     */
    public function getSessionsByConference(Conference $conference)
    {
        return $this->getSessionsForNode($conference->getNode());
    }

    /**
     * @param Conference $conference
     * @param string $sessionType @see Session::SESSION_TYPE_OPTIONS
     * @return Session[]
     */
    public function getSessionsByConferenceAndSessionType(Conference $conference, string $sessionType)
    {
        $nodeService = $this->nodeServiceFactory->forNode($conference->getNode());
        $sessionFilter = new SessionFilter([
            'sessionType' => $sessionType
        ]);
        $sessionNodes = $nodeService->getNodesByFilter($sessionFilter);

        return array_map(function (NodeInterface $node) {
            return new Session($node);
        }, $sessionNodes);
    }

}