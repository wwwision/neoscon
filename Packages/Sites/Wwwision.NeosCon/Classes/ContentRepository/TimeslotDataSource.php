<?php
namespace Wwwision\NeosCon\ContentRepository;

use Neos\Flow\Annotations as Flow;
use Neos\Neos\Service\DataSource\AbstractDataSource;
use Neos\ContentRepository\Domain\Model\NodeInterface;

/**
 * @Flow\Scope("singleton")
 */
final class TimeslotDataSource extends AbstractDataSource
{

    static protected $identifier = 'wwwision-neoscon-timeslots';

    public function getData(NodeInterface $node = null, array $options)
    {
        return [
            ['value' => '1-0930', 'label' => '9:30', 'group' => 'Friday', 'icon' => 'icon-clock'],
            ['value' => '1-1015', 'label' => '10:15', 'group' => 'Friday', 'icon' => 'icon-clock'],
            ['value' => '1-1100', 'label' => '11:00', 'group' => 'Friday', 'icon' => 'icon-clock'],
            ['value' => '1-1130', 'label' => '11:30', 'group' => 'Friday', 'icon' => 'icon-clock'],
            ['value' => '1-1215', 'label' => '12:15', 'group' => 'Friday', 'icon' => 'icon-clock'],
            ['value' => '2-1000', 'label' => '10:00', 'group' => 'Saturday', 'icon' => 'icon-clock'],
            ['value' => '2-1045', 'label' => '10:45', 'group' => 'Saturday', 'icon' => 'icon-clock'],
            ['value' => '2-1130', 'label' => '11:30', 'group' => 'Saturday', 'icon' => 'icon-clock'],
            ['value' => '2-1145', 'label' => '11:45', 'group' => 'Saturday', 'icon' => 'icon-clock'],
        ];
    }
}