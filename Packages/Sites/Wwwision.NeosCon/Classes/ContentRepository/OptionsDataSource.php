<?php
namespace Wwwision\NeosCon\ContentRepository;

use Neos\Flow\Annotations as Flow;
use Neos\Neos\Service\DataSource\AbstractDataSource;
use Neos\ContentRepository\Domain\Model\NodeInterface;
use Wwwision\NeosCon\Domain\Model\Session;

/**
 * @Flow\Scope("singleton")
 */
final class OptionsDataSource extends AbstractDataSource
{

    static protected $identifier = 'wwwision-neoscon-options';

    public function getData(NodeInterface $node = null, array $options)
    {
        $selectItems = [];
        if (isset($options['type']) && $options['type'] === 'sessionType') {
            $selectItems = Session::SESSION_TYPE_OPTIONS;
        }
        return array_map(function($value) { return ['label' => $value]; }, $selectItems);
    }

}