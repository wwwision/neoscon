<?php
namespace Wwwision\NeosCon\Security;

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Security\Account;
use Neos\Flow\Security\AccountRepository;
use Neos\Flow\Security\Context;
use Neos\Flow\Security\Cryptography\HashService;
use Neos\Flow\Security\Policy\PolicyService;

/**
 * @Flow\Scope("singleton")
 * @internal This service is meant to be used by other services exclusively
 */
class AccountService
{

    const AUTHENTICATION_PROVIDER_NAME = 'Wwwision.NeosCon:Frontend';

    /**
     * @var AccountRepository
     */
    private $accountRepository;

    /**
     * @var Context
     */
    private $securityContext;

    /**
     * @var PolicyService
     */
    private $policyService;

    /**
     * @var HashService
     */
    private $hashService;

    /**
     * @param AccountRepository $accountRepository
     * @param Context $securityContext
     * @param PolicyService $policyService
     * @param HashService $hashService
     */
    public function __construct(AccountRepository $accountRepository, Context $securityContext, PolicyService $policyService, HashService $hashService)
    {
        $this->accountRepository = $accountRepository;
        $this->securityContext = $securityContext;
        $this->policyService = $policyService;
        $this->hashService = $hashService;
    }


    /**
     * @param string $identifier
     * @param string $password plaintext password
     * @param string[] $roleIdentifiers
     * @return Account
     */
    public function createAccount($identifier, $password, array $roleIdentifiers)
    {
        $account = new Account();
        $account->setAccountIdentifier($identifier);
        $account->setCredentialsSource($this->hashService->hashPassword($password));
        $account->setAuthenticationProviderName(self::AUTHENTICATION_PROVIDER_NAME);
        foreach ($roleIdentifiers as $roleIdentifier) {
            $account->addRole($this->policyService->getRole($roleIdentifier));
        }
        $this->accountRepository->add($account);
        return $account;
    }

    /**
     * @return Account
     */
    public function getAuthenticatedAccount()
    {
        return $this->securityContext->getAccountByAuthenticationProviderName(self::AUTHENTICATION_PROVIDER_NAME);
    }

    /**
     * @param string $accountIdentifier
     * @return Account
     */
    public function getAccountByIdentifier(string $accountIdentifier)
    {
        $account = null;
        $this->securityContext->withoutAuthorizationChecks(function () use ($accountIdentifier, &$account) {
            $account = $this->accountRepository->findActiveByAccountIdentifierAndAuthenticationProviderName($accountIdentifier, self::AUTHENTICATION_PROVIDER_NAME);
        });
        return $account;
    }
}