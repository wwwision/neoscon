<?php
namespace Wwwision\NeosCon\Domain\Model;

use Neos\Flow\Annotations as Flow;

final class Speaker extends AbstractNodeBasedModel
{
    public function getFullName(): string
    {
        return $this['givenName'] . ' ' . $this['familyName'];
    }

}