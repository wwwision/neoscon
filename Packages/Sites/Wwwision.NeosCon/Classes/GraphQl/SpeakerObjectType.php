<?php
namespace Wwwision\NeosCon\GraphQl;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use Neos\Flow\Annotations as Flow;
use Wwwision\GraphQL\TypeResolver;
use Wwwision\NeosCon\Domain\Model\Speaker;

final class SpeakerObjectType extends ObjectType
{

    /**
     * @param TypeResolver $typeResolver
     */
    public function __construct(TypeResolver $typeResolver)
    {
        return parent::__construct([
            'name' => 'Speaker',
            'fields' => [
                'identifier' => ['type' => Type::string()],
                'fullName' => ['type' => Type::string()],
                'test' => [
                    'type' => Type::string(),
                    'resolve' => function(Speaker $speaker)
                    {
                        return 'AAAA';
                    }
                ],
            ]
        ]);
    }
}