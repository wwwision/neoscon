<?php
namespace Wwwision\NeosCon\Security;

use Neos\Flow\Mvc\ActionRequest;
use Neos\Flow\Mvc\RequestInterface;
use Neos\Flow\Security\RequestPatternInterface;

/**
 * A request pattern that can detect and match "frontend" and "backend" mode
 */
class NeosRequestPattern implements RequestPatternInterface {

	const PATTERN_BACKEND = 'backend';
	const PATTERN_FRONTEND = 'frontend';

	/**
	 * @var boolean
	 */
	protected $shouldMatchBackend = TRUE;

	/**
	 * Returns the set pattern
	 *
	 * @return string The set pattern
	 */
	public function getPattern() {
		return $this->shouldMatchBackend ? self::PATTERN_BACKEND : self::PATTERN_FRONTEND;
	}

	/**
	 * Sets the pattern (match) configuration
	 *
	 * @param object $pattern The pattern (match) configuration
	 * @return void
	 */
	public function setPattern($pattern) {
		$this->shouldMatchBackend = ($pattern === self::PATTERN_FRONTEND) ? FALSE : TRUE;
	}

	/**
	 * Matches a \Neos\Flow\Mvc\RequestInterface against its set pattern rules
	 *
	 * @param RequestInterface $request The request that should be matched
	 * @return boolean TRUE if the pattern matched, FALSE otherwise
	 */
	public function matchRequest(RequestInterface $request) {
		if (!$request instanceof ActionRequest) {
			return FALSE;
		}
		$requestPath = $request->getHttpRequest()->getUri()->getPath();
		$requestPathMatchesBackend = preg_match('/^\/neos($|\/)|\@/', $requestPath) === 1;
		return $this->shouldMatchBackend === $requestPathMatchesBackend;
	}

}