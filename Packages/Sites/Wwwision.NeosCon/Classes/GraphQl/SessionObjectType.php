<?php
namespace Wwwision\NeosCon\GraphQl;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use Neos\Flow\Annotations as Flow;
use Wwwision\GraphQL\TypeResolver;
use Wwwision\NeosCon\GraphQl\Scalars\SessionTypeEnum;

final class SessionObjectType extends ObjectType
{

    /**
     * @param TypeResolver $typeResolver
     */
    public function __construct(TypeResolver $typeResolver)
    {
        return parent::__construct([
            'name' => 'Session',
            'fields' => [
                'title' => ['type' => Type::string()],
                'abstract' => ['type' => Type::string()],
                'sessionType' => ['type' => $typeResolver->get(SessionTypeEnum::class)],
                'speakers' => ['type' => Type::listOf($typeResolver->get(SpeakerObjectType::class))],
            ],
        ]);
    }
}