<?php
namespace Wwwision\NeosCon\GraphQl;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use Wwwision\GraphQL\TypeResolver;
use Wwwision\NeosCon\GraphQl\Scalars\DateTime;

final class RoomObjectType extends ObjectType
{

    /**
     * @param TypeResolver $typeResolver
     */
    public function __construct(TypeResolver $typeResolver)
    {
        return parent::__construct([
            'name' => 'Room',
            'fields' => [
                'title' => ['type' => Type::string()],
                'numberOfSeats' => ['type' => Type::int()],
            ]
        ]);
    }
}