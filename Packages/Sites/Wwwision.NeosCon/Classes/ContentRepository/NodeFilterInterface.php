<?php
namespace Wwwision\NeosCon\ContentRepository;

interface NodeFilterInterface
{
    public function getNodeType(): string;

    public function getFilterString(): string;
}