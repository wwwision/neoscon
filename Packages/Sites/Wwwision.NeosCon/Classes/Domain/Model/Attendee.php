<?php
namespace Wwwision\NeosCon\Domain\Model;

use Doctrine\ORM\Mapping as ORM;
use Neos\Flow\Annotations as Flow;

/**
 * A conference attendee (Note: This model is _not_ based on a Content Repository node, but it's a regular entity)
 *
 * @Flow\Entity
 */
class Attendee
{
    /**
     * @var string
     * @ORM\Id
     */
    protected $accountId;

    /**
     * @var string
     */
    protected $fullName;

    public function __construct(string $accountId, string $fullName)
    {
        $this->accountId = $accountId;
        $this->fullName = $fullName;
    }

    public function getAccountId(): string
    {
        return $this->accountId;
    }

    public function getFullName(): string
    {
        return $this->fullName;
    }

}