<?php
namespace Wwwision\NeosCon\Eel\Helper;

use Neos\Eel\ProtectedContextAwareInterface;
use Neos\Flow\Annotations as Flow;
use Wwwision\NeosCon\Domain\Service\ConferenceService;

/**
 * @Flow\Scope("singleton")
 */
class ConferenceHelper implements ProtectedContextAwareInterface
{

    /**
     * @var ConferenceService
     */
    private $conferenceService;

    public function __construct(ConferenceService $conferenceService)
    {
        $this->conferenceService = $conferenceService;
    }

    /**
     * @param string $methodName
     * @param array $arguments
     * @return mixed
     */
    public function __call(string $methodName, array $arguments)
    {
        $methodName = 'get' . ucfirst($methodName);
        return call_user_func_array([$this->conferenceService, $methodName], $arguments);
    }

    /**
     * @param string $methodName
     * @return boolean
     */
    public function allowsCallOfMethod($methodName)
    {
        return true;
    }
}