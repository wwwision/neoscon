<?php
namespace Wwwision\NeosCon\Eel\Helper;

use Neos\Eel\ProtectedContextAwareInterface;
use Neos\Flow\Annotations as Flow;
use Wwwision\NeosCon\Domain\Service\SessionService;

/**
 * @Flow\Scope("singleton")
 */
class SessionHelper implements ProtectedContextAwareInterface
{

    /**
     * @var SessionService
     */
    private $sessionService;

    public function __construct(SessionService $sessionService)
    {
        $this->sessionService = $sessionService;
    }

    /**
     * @param string $methodName
     * @param array $arguments
     * @return mixed
     */
    public function __call(string $methodName, array $arguments)
    {
        $methodName = 'get' . ucfirst($methodName);
        return call_user_func_array([$this->sessionService, $methodName], $arguments);
    }

    /**
     * @param string $methodName
     * @return boolean
     */
    public function allowsCallOfMethod($methodName)
    {
        return true;
    }
}