<?php
namespace Wwwision\NeosCon\ContentRepository\Service;

use Neos\Eel\FlowQuery\FlowQuery;
use Neos\Flow\Annotations as Flow;
use Neos\Neos\Domain\Service\ContentContext;
use Neos\ContentRepository\Domain\Model\NodeInterface;
use Wwwision\NeosCon\ContentRepository\NodeFilterInterface;

/**
 * @internal This service is meant to be instantiated using the NodeServiceFactory and used by other services exclusively
 */
class NodeService
{

    /**
     * @var ContentContext
     */
    private $contentContext;

    /**
     * @param ContentContext $contentContext
     */
    public function __construct(ContentContext $contentContext)
    {
        $this->contentContext = $contentContext;
    }

    /**
     * @param string $nodeType
     * @param NodeInterface $rootNode optional root node
     * @return NodeInterface[]
     */
    public function getNodesByType(string $nodeType, NodeInterface $rootNode = null): array
    {
        if ($rootNode === null) {
            $rootNode = $this->contentContext->getRootNode();
        }
        $flowQuery = new FlowQuery([$rootNode]);
        /** @noinspection PhpUndefinedMethodInspection */
        return $flowQuery->find('[instanceof ' . $nodeType . ']')->get();
    }

    /**
     * @param NodeFilterInterface $nodeFilter
     * @return NodeInterface[]
     */
    public function getNodesByFilter(NodeFilterInterface $nodeFilter)
    {
        $rootNode = null;//$nodeFilter->getRootNode();
        if ($rootNode === null) {
            $rootNode = $this->contentContext->getRootNode();
        }
        $flowQuery = new FlowQuery([$rootNode]);
        /** @noinspection PhpUndefinedMethodInspection */
        return $flowQuery->find('[instanceof ' . $nodeFilter->getNodeType() . ']')->filter($nodeFilter->getFilterString())->get();
    }

    /**
     * @param string $nodeType
     * @param NodeInterface $rootNode Optional root node, if not set the global rootNode will be used
     * @return NodeInterface
     */
    public function getFirstNodeByType($nodeType, NodeInterface $rootNode = null)
    {
        if ($rootNode === null) {
            $rootNode = $this->contentContext->getRootNode();
        }
        $flowQuery = new FlowQuery([$rootNode]);
        /** @noinspection PhpUndefinedMethodInspection */
        return $flowQuery->find('[instanceof ' . $nodeType . ']')->get(0);
    }
    /**
     * @param NodeFilterInterface $nodeFilter
     * @param NodeInterface $rootNode Optional root node, if not set the global rootNode will be used
     * @return NodeInterface
     */
    public function getNodeByFilter(NodeFilterInterface $nodeFilter, NodeInterface $rootNode = null)
    {
        if ($rootNode === null) {
            $rootNode = $this->contentContext->getRootNode();
        }
        $flowQuery = new FlowQuery([$rootNode]);
        /** @noinspection PhpUndefinedMethodInspection */
        return $flowQuery->find('[instanceof ' . $nodeFilter->getNodeType() . ']')->filter($nodeFilter->getFilterString())->get(0);
    }
    /**
     * @param NodeInterface $referenceNode reference node
     * @param string $nodeType
     * @return NodeInterface
     */
    public function getClosestAncestorNodeByType(NodeInterface $referenceNode, $nodeType)
    {
        $flowQuery = new FlowQuery([$referenceNode]);
        /** @noinspection PhpUndefinedMethodInspection */
        return $flowQuery->closest('[instanceof ' . $nodeType . ']')->get(0);
    }
    /**
     * @param string $identifier
     * @return NodeInterface
     */
    public function getNodeByIdentifier($identifier)
    {
        return $this->contentContext->getNodeByIdentifier($identifier);
    }
}