<?php
namespace Wwwision\NeosCon\GraphQl\RootTypes;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use Neos\Flow\Annotations as Flow;
use Wwwision\GraphQL\TypeResolver;
use Wwwision\NeosCon\Domain\Service\AttendeeService;
use Wwwision\NeosCon\Domain\Service\ConferenceService;
use Wwwision\NeosCon\GraphQl\AttendeeObjectType;
use Wwwision\NeosCon\GraphQl\ConferenceObjectType;

class Query extends ObjectType
{
    /**
     * @Flow\Inject
     * @var ConferenceService
     */
    protected $conferenceService;

    /**
     * @Flow\Inject
     * @var AttendeeService
     */
    protected $attendeeService;

    /**
     * @param TypeResolver $typeResolver
     */
    public function __construct(TypeResolver $typeResolver)
    {
        return parent::__construct([
            'name' => 'Query',
            'fields' => [
                'conferences' => [
                    'type' => Type::listOf($typeResolver->get(ConferenceObjectType::class)),
                    'resolve' => function () {
                        return $this->conferenceService->getConferences();
                    },
                ],
                'attendee' => [
                    'type' => $typeResolver->get(AttendeeObjectType::class),
                    'resolve' => function () {
                        return $this->attendeeService->getAuthenticatedAttendee();
                    },
                ],
            ],
        ]);
    }
}