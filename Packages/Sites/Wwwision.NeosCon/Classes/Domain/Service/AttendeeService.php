<?php
namespace Wwwision\NeosCon\Domain\Service;

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Security\Exception\AccessDeniedException;
use Wwwision\Likes\LikeService;
use Wwwision\NeosCon\Domain\Model\Attendee;
use Wwwision\NeosCon\Domain\Repository\AttendeeRepository;
use Wwwision\NeosCon\Security\AccountService;

/**
 * @Flow\Scope("singleton")
 */
final class AttendeeService
{
    /**
     * @var AttendeeRepository
     */
    private $attendeeRepository;

    /**
     * @var AccountService
     */
    private $accountService;

    /**
     * @var LikeService
     */
    private $likeService;

    public function __construct(AttendeeRepository $attendeeRepository, AccountService $accountService, LikeService $likeService)
    {
        $this->attendeeRepository = $attendeeRepository;
        $this->accountService = $accountService;
        $this->likeService = $likeService;
    }

    public function createAttendee(string $fullName, string $username, string $password)
    {
        $account = $this->accountService->createAccount($username, $password, ['Wwwision.NeosCon:Attendee']);
        $attendee = new Attendee($account->getAccountIdentifier(), $fullName);
        $this->attendeeRepository->add($attendee);
    }

    public function getAuthenticatedAttendee()
    {
        $authenticatedAccount = $this->accountService->getAuthenticatedAccount();
        if ($authenticatedAccount === null) {
            return null;
        }
        return $this->attendeeRepository->findOneByAccountId($authenticatedAccount->getAccountIdentifier());
    }

    public function addSessionLikeForAuthenticatedAttendee(string $sessionId)
    {
        $attendee = $this->getAuthenticatedAttendee();
        if ($attendee === null) {
            throw new AccessDeniedException('This action requires an authenticated attendee!', 1489143921);
        }
        $this->likeService->addLike('Session', $attendee->getAccountId(), $sessionId);
    }

    public function revokeSessionLikeForAuthenticatedAttendee(string $sessionId)
    {
        $attendee = $this->getAuthenticatedAttendee();
        if ($attendee === null) {
            throw new AccessDeniedException('This action requires an authenticated attendee!', 1489143922);
        }
        $this->likeService->revokeLike('Session', $attendee->getAccountId(), $sessionId);
    }

    /**
     * Whether the given $attendee liked the session with id $sessionId
     */
    public function attendeeLikesSession(Attendee $attendee, string $sessionId): bool
    {
        return $this->likeService->likeExists('Session', $attendee->getAccountId(), $sessionId);
    }
}