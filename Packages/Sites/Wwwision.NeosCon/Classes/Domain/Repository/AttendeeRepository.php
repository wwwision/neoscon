<?php
namespace Wwwision\NeosCon\Domain\Repository;

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Persistence\Repository;
use Wwwision\NeosCon\Domain\Model\Attendee;

/**
 * @internal Only to be used by the AttendeeService
 * @Flow\Scope("singleton")
 */
final class AttendeeRepository extends Repository
{

    /**
     * @param string $accountId
     * @return Attendee
     */
    public function findOneByAccountId(string $accountId)
    {
        $query = $this->createQuery();
        /** @var Attendee $attendee */
        $attendee = $query->matching(
                $query->equals('accountId', $accountId)
            )
            ->execute()
            ->getFirst();
        return $attendee;
    }
}