<?php
namespace Wwwision\NeosCon\Eel\Helper;

use Neos\Eel\ProtectedContextAwareInterface;
use Neos\Flow\Annotations as Flow;
use Wwwision\NeosCon\Domain\Service\VenueService;

/**
 * @Flow\Scope("singleton")
 */
class VenueHelper implements ProtectedContextAwareInterface
{

    /**
     * @var VenueService
     */
    private $venueService;

    public function __construct(VenueService $venueService)
    {
        $this->venueService = $venueService;
    }

    /**
     * @param string $methodName
     * @param array $arguments
     * @return mixed
     */
    public function __call(string $methodName, array $arguments)
    {
        $methodName = 'get' . ucfirst($methodName);
        return call_user_func_array([$this->venueService, $methodName], $arguments);
    }

    /**
     * @param string $methodName
     * @return boolean
     */
    public function allowsCallOfMethod($methodName)
    {
        return true;
    }
}